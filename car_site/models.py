# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.conf.urls.static import static


def detect_cars(image_name):
    """
        Method is responsible of extracting cars from the uploaded images. Then, the extracted cars will be saved into media/output folder.

    :param image_name: Input image that is uploaded to web page by user
    :return: Saved images which contains only the car part of the uploaded image
    """
    import tensorflow as tf
    from imageai.Detection import ObjectDetection

    # Let Tensorflow to use 0.333 as per process gpu memory
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)

    # Create a Tensorflow session for better memory management
    sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
    with sess:
        model_path = "car_site/models/yolo.h5"
        input_path = "./media/" + image_name
        output_path = "./media/output/" + image_name

        detector = ObjectDetection()
        detector.setModelTypeAsYOLOv3()
        detector.setModelPath(model_path)
        detector.loadModel()
        custom = detector.CustomObjects(car=True)

        detector.detectCustomObjectsFromImage(custom_objects=custom,
                                              input_image=input_path,
                                              output_image_path=output_path,
                                              minimum_percentage_probability=80,
                                              extract_detected_objects=True)


def classify_cars(image_path, model_name):
    """
        Method is responsible of using the extracted car image to classify car brand or type based on the selected model.
        All available models are explained in the 'CAR DC.docx' document which is located under the project folder.

    :param image_path: Extracted car image path
    :param model_name: Model that will be used to classify cars
    :return: Text output on the web page which will label the predicted top3 classes with their probabilities.
    """
    import torch
    from torchvision import transforms
    from PIL import Image

    classifier_path = "car_site/models/" + model_name + ".model"

    model = torch.load(classifier_path)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model.to(device)

    # switch the model to evaluation mode to make dropout and batch norm work in eval mode
    model.eval()

    # To have more robust predictions, apply some 'real life' transformation
    # Same transformation has been used on test data during the training.
    image_transform = transforms.Compose([transforms.Resize((400, 400)),
                                          transforms.ToTensor(),
                                          transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    image = Image.open(image_path)
    image = image_transform(image).float()
    image = torch.autograd.Variable(image, requires_grad=True)
    image = image.unsqueeze(0)
    image = image.cuda()
    output = model(image)

    # Below dictionary is the representation of our dataset which includes the trained model names as dictionary key, and corresponding car model names as values,
    # dictionary structure: { 'trained_model_name': [list of car brand names which were used during training (namely network output classes] }
    output_class_dict = {
        '196_epoch_8': ['AM General Hummer SUV 2000', 'Acura Integra Type R 2001', 'Acura RL Sedan 2012', 'Acura TL Sedan 2012', 'Acura TL Type-S 2008', 'Acura TSX Sedan 2012',
                        'Acura ZDX Hatchback 2012', 'Aston Martin V8 Vantage Convertible 2012', 'Aston Martin V8 Vantage Coupe 2012', 'Aston Martin Virage Convertible 2012',
                        'Aston Martin Virage Coupe 2012', 'Audi 100 Sedan 1994', 'Audi 100 Wagon 1994', 'Audi A5 Coupe 2012', 'Audi R8 Coupe 2012', 'Audi RS 4 Convertible 2008',
                        'Audi S4 Sedan 2007',
                        'Audi S4 Sedan 2012', 'Audi S5 Convertible 2012', 'Audi S5 Coupe 2012', 'Audi S6 Sedan 2011', 'Audi TT Hatchback 2011', 'Audi TT RS Coupe 2012',
                        'Audi TTS Coupe 2012',
                        'Audi V8 Sedan 1994', 'BMW 1 Series Convertible 2012', 'BMW 1 Series Coupe 2012', 'BMW 3 Series Sedan 2012', 'BMW 3 Series Wagon 2012',
                        'BMW 6 Series Convertible 2007',
                        'BMW ActiveHybrid 5 Sedan 2012', 'BMW M3 Coupe 2012', 'BMW M5 Sedan 2010', 'BMW M6 Convertible 2010', 'BMW X3 SUV 2012', 'BMW X5 SUV 2007',
                        'BMW X6 SUV 2012',
                        'BMW Z4 Convertible 2012', 'Bentley Arnage Sedan 2009', 'Bentley Continental Flying Spur Sedan 2007', 'Bentley Continental GT Coupe 2007',
                        'Bentley Continental GT Coupe 2012',
                        'Bentley Continental Supersports Conv. Convertible 2012', 'Bentley Mulsanne Sedan 2011', 'Bugatti Veyron 16.4 Convertible 2009',
                        'Bugatti Veyron 16.4 Coupe 2009',
                        'Buick Enclave SUV 2012', 'Buick Rainier SUV 2007', 'Buick Regal GS 2012', 'Buick Verano Sedan 2012', 'Cadillac CTS-V Sedan 2012',
                        'Cadillac Escalade EXT Crew Cab 2007',
                        'Cadillac SRX SUV 2012', 'Chevrolet Avalanche Crew Cab 2012', 'Chevrolet Camaro Convertible 2012', 'Chevrolet Cobalt SS 2010',
                        'Chevrolet Corvette Convertible 2012',
                        'Chevrolet Corvette Ron Fellows Edition Z06 2007', 'Chevrolet Corvette ZR1 2012', 'Chevrolet Express Cargo Van 2007', 'Chevrolet Express Van 2007',
                        'Chevrolet HHR SS 2010',
                        'Chevrolet Impala Sedan 2007', 'Chevrolet Malibu Hybrid Sedan 2010', 'Chevrolet Malibu Sedan 2007', 'Chevrolet Monte Carlo Coupe 2007',
                        'Chevrolet Silverado 1500 Classic Extended Cab 2007', 'Chevrolet Silverado 1500 Extended Cab 2012', 'Chevrolet Silverado 1500 Hybrid Crew Cab 2012',
                        'Chevrolet Silverado 1500 Regular Cab 2012', 'Chevrolet Silverado 2500HD Regular Cab 2012', 'Chevrolet Sonic Sedan 2012', 'Chevrolet Tahoe Hybrid SUV 2012',
                        'Chevrolet TrailBlazer SS 2009', 'Chevrolet Traverse SUV 2012', 'Chrysler 300 SRT-8 2010', 'Chrysler Aspen SUV 2009', 'Chrysler Crossfire Convertible 2008',
                        'Chrysler PT Cruiser Convertible 2008', 'Chrysler Sebring Convertible 2010', 'Chrysler Town and Country Minivan 2012', 'Daewoo Nubira Wagon 2002',
                        'Dodge Caliber Wagon 2007',
                        'Dodge Caliber Wagon 2012', 'Dodge Caravan Minivan 1997', 'Dodge Challenger SRT8 2011', 'Dodge Charger SRT-8 2009', 'Dodge Charger Sedan 2012',
                        'Dodge Dakota Club Cab 2007',
                        'Dodge Dakota Crew Cab 2010', 'Dodge Durango SUV 2007', 'Dodge Durango SUV 2012', 'Dodge Journey SUV 2012', 'Dodge Magnum Wagon 2008',
                        'Dodge Ram Pickup 3500 Crew Cab 2010',
                        'Dodge Ram Pickup 3500 Quad Cab 2009', 'Dodge Sprinter Cargo Van 2009', 'Eagle Talon Hatchback 1998', 'FIAT 500 Abarth 2012', 'FIAT 500 Convertible 2012',
                        'Ferrari 458 Italia Convertible 2012', 'Ferrari 458 Italia Coupe 2012', 'Ferrari California Convertible 2012', 'Ferrari FF Coupe 2012',
                        'Fisker Karma Sedan 2012',
                        'Ford E-Series Wagon Van 2012', 'Ford Edge SUV 2012', 'Ford Expedition EL SUV 2009', 'Ford F-150 Regular Cab 2007', 'Ford F-150 Regular Cab 2012',
                        'Ford F-450 Super Duty Crew Cab 2012', 'Ford Fiesta Sedan 2012', 'Ford Focus Sedan 2007', 'Ford Freestar Minivan 2007', 'Ford GT Coupe 2006',
                        'Ford Mustang Convertible 2007',
                        'Ford Ranger SuperCab 2011', 'GMC Acadia SUV 2012', 'GMC Canyon Extended Cab 2012', 'GMC Savana Van 2012', 'GMC Terrain SUV 2012',
                        'GMC Yukon Hybrid SUV 2012',
                        'Geo Metro Convertible 1993', 'HUMMER H2 SUT Crew Cab 2009', 'HUMMER H3T Crew Cab 2010', 'Honda Accord Coupe 2012', 'Honda Accord Sedan 2012',
                        'Honda Odyssey Minivan 2007',
                        'Honda Odyssey Minivan 2012', 'Hyundai Accent Sedan 2012', 'Hyundai Azera Sedan 2012', 'Hyundai Elantra Sedan 2007',
                        'Hyundai Elantra Touring Hatchback 2012',
                        'Hyundai Genesis Sedan 2012', 'Hyundai Santa Fe SUV 2012', 'Hyundai Sonata Hybrid Sedan 2012', 'Hyundai Sonata Sedan 2012', 'Hyundai Tucson SUV 2012',
                        'Hyundai Veloster Hatchback 2012', 'Hyundai Veracruz SUV 2012', 'Infiniti G Coupe IPL 2012', 'Infiniti QX56 SUV 2011', 'Isuzu Ascender SUV 2008',
                        'Jaguar XK XKR 2012',
                        'Jeep Compass SUV 2012', 'Jeep Grand Cherokee SUV 2012', 'Jeep Liberty SUV 2012', 'Jeep Patriot SUV 2012', 'Jeep Wrangler SUV 2012',
                        'Lamborghini Aventador Coupe 2012',
                        'Lamborghini Diablo Coupe 2001', 'Lamborghini Gallardo LP 570-4 Superleggera 2012', 'Lamborghini Reventon Coupe 2008', 'Land Rover LR2 SUV 2012',
                        'Land Rover Range Rover SUV 2012', 'Lincoln Town Car Sedan 2011', 'MINI Cooper Roadster Convertible 2012', 'Maybach Landaulet Convertible 2012',
                        'Mazda Tribute SUV 2011',
                        'McLaren MP4-12C Coupe 2012', 'Mercedes-Benz 300-Class Convertible 1993', 'Mercedes-Benz C-Class Sedan 2012', 'Mercedes-Benz E-Class Sedan 2012',
                        'Mercedes-Benz S-Class Sedan 2012', 'Mercedes-Benz SL-Class Coupe 2009', 'Mercedes-Benz Sprinter Van 2012', 'Mitsubishi Lancer Sedan 2012',
                        'Nissan 240SX Coupe 1998',
                        'Nissan Juke Hatchback 2012', 'Nissan Leaf Hatchback 2012', 'Nissan NV Passenger Van 2012', 'Plymouth Neon Coupe 1999', 'Porsche Panamera Sedan 2012',
                        'Ram C-V Cargo Van Minivan 2012', 'Rolls-Royce Ghost Sedan 2012', 'Rolls-Royce Phantom Drophead Coupe Convertible 2012', 'Rolls-Royce Phantom Sedan 2012',
                        'Scion xD Hatchback 2012', 'Spyker C8 Convertible 2009', 'Spyker C8 Coupe 2009', 'Suzuki Aerio Sedan 2007', 'Suzuki Kizashi Sedan 2012',
                        'Suzuki SX4 Hatchback 2012',
                        'Suzuki SX4 Sedan 2012', 'Tesla Model S Sedan 2012', 'Toyota 4Runner SUV 2012', 'Toyota Camry Sedan 2012', 'Toyota Corolla Sedan 2012',
                        'Toyota Sequoia SUV 2012',
                        'Volkswagen Beetle Hatchback 2012', 'Volkswagen Golf Hatchback 1991', 'Volkswagen Golf Hatchback 2012', 'Volvo 240 Sedan 1993', 'Volvo C30 Hatchback 2012',
                        'Volvo XC90 SUV 2007', 'smart fortwo Convertible 2012'],

        'car_type_epoch_8': ['SEDAN', 'SUV', 'VAN'],

        'chevrolet_epoch_8': ['Chevrolet Avalanche Crew Cab 2012', 'Chevrolet Camaro Convertible 2012', 'Chevrolet Cobalt SS 2010', 'Chevrolet Corvette Convertible 2012',
                              'Chevrolet Corvette Ron Fellows Edition Z06 2007', 'Chevrolet Corvette ZR1 2012', 'Chevrolet Express Cargo Van 2007', 'Chevrolet Express Van 2007',
                              'Chevrolet HHR SS 2010',
                              'Chevrolet Impala Sedan 2007', 'Chevrolet Malibu Hybrid Sedan 2010', 'Chevrolet Malibu Sedan 2007', 'Chevrolet Monte Carlo Coupe 2007',
                              'Chevrolet Silverado 1500 Classic Extended Cab 2007', 'Chevrolet Silverado 1500 Extended Cab 2012', 'Chevrolet Silverado 1500 Hybrid Crew Cab 2012',
                              'Chevrolet Silverado 1500 Regular Cab 2012', 'Chevrolet Silverado 2500HD Regular Cab 2012', 'Chevrolet Sonic Sedan 2012',
                              'Chevrolet Tahoe Hybrid SUV 2012',
                              'Chevrolet TrailBlazer SS 2009', 'Chevrolet Traverse SUV 2012'],

        'mercedes_epoch_8': ['Mercedes-Benz 300-Class Convertible 1993', 'Mercedes-Benz C-Class Sedan 2012', 'Mercedes-Benz E-Class Sedan 2012', 'Mercedes-Benz S-Class Sedan 2012',
                             'Mercedes-Benz SL-Class Coupe 2009', 'Mercedes-Benz Sprinter Van 2012']}

    # Get the confident and labels for TOP3 predictions
    top_k_confident, top_k_label = torch.topk(output, 3)

    # In order to get 'human readable' confident percentage, we need to convert tensors to numpy arrays.
    output_confident = torch.nn.functional.softmax(top_k_confident, dim=1).cpu().detach().numpy()
    top_k_label = top_k_label.cpu().detach().numpy()

    # Create a string from the TOP3 predictions and return it to web page.
    result_string = 'Top 3 Predicted Classes: \n'
    for confident, label in zip(output_confident[0], top_k_label[0]):
        result_string += output_class_dict[model_name][label] + ' %' + str(round(confident * 100, 3)) + ' \n'

    return result_string
