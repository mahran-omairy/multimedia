# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
from django.utils.datastructures import MultiValueDictKeyError

from .models import classify_cars, detect_cars

import os

def index(request):
    """
        method will handle the upcoming request which is an image that user is trying to upload. Then image will be extracted if it has car. After the extraction,
        image will be saved under media/output folder then it will be classified based on the selected classifier model. Classification output will be appear as
        a text on the web page.
    :param request: request object
    :return: Renders the web page view
    """
    context = {}
    try:
        if request.method == 'POST' and request.FILES['image']:
            model_name = request.POST.get('model_picker', '')

            my_file = request.FILES['image']
            fs = FileSystemStorage()
            filename = fs.save(my_file.name, my_file)
            detect_cars(filename)
            uploaded_file_path = f'./media/output/' + filename + '-objects/'

            results = []
            for image in os.listdir(uploaded_file_path):
                if image.endswith('.jpg'):
                    car_name = classify_cars(uploaded_file_path + image, model_name)
                    result_dictionary = {'uploaded_file_path':f'/media/output/' + filename + '-objects/' + image, 'car_name':car_name}
                    results.append(result_dictionary)

            context['result'] = results

    except FileNotFoundError as file_not_found:
        context['error'] = 'YOLOv3 Image Detector is not able to find any car in the uploaded image..'

    except MultiValueDictKeyError as key_error:
        context['error'] = 'Please consider uploading an image...'

    except Exception as err:
        context['error'] = 'Something went wrong...'

    return render(request, 'home/index.html', context)
