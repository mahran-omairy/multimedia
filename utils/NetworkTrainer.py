import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.models as models
import torchvision.transforms as transforms

import datetime
import logging
import time
import os

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('NetworkTrainer.py')

def log_event(message):
    current_time = datetime.datetime.now()
    logger.info(message + ' >> ' + str(current_time.strftime('%d-%m-%Y %H-%M-%S')))

# Set the device
device = torch.device("cuda:0")

"""
dataset_path must point to the source dataset

# Path for all Car models (196 classes)
dataset_path = f"D:/multimedia_project/stanford-car-dataset-by-classes-folder/car_data/car_data/"

#Path for Mercedes (one car - 6 classes)
dataset_path = f"D:/multimedia_project/one_car/"

#Path for Chevrolet (one car - 22 classes)
dataset_path = f"D:/multimedia_project/che_one_car/"


dataset_path = f"D:/multimedia_project/stanford-car-dataset-by-classes-folder/car_data/car_data/"
"""

dataset_path = f"D:/multimedia_project/SUV_vs_SEDAN_vs_VAN/"

train_transformations = transforms.Compose([transforms.Resize((400, 400)),
                                            transforms.RandomHorizontalFlip(),
                                            transforms.RandomRotation(15),
                                            transforms.ToTensor(),
                                            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

test_transformations = transforms.Compose([transforms.Resize((400, 400)),
                                           transforms.ToTensor(),
                                           transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

train_data = torchvision.datasets.ImageFolder(root=dataset_path + "train", transform=train_transformations)
train_loader = torch.utils.data.DataLoader(train_data, batch_size=32, shuffle=True, num_workers=8)

test_data = torchvision.datasets.ImageFolder(root=dataset_path + "test", transform=test_transformations)
test_loader = torch.utils.data.DataLoader(test_data, batch_size=32, shuffle=False, num_workers=8)


def train_model(model, criterion, optimizer, scheduler, n_epochs=16):
    losses = []
    accuracies = []
    test_accuracies = []

    # set the model to train mode initially
    model.train()

    for epoch in range(n_epochs):
        since = time.time()
        running_loss = 0.0
        running_correct = 0.0

        for i, data in enumerate(train_loader, 0):
            # Assign the upcoming input(s) to CUDA
            inputs, labels = data

            inputs = inputs.to(device)
            labels = labels.to(device)
            optimizer.zero_grad()

            outputs = model(inputs)
            _, predicted = torch.max(outputs.data, 1)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            # calculate the loss/acc later
            running_loss += loss.item()
            running_correct += (labels == predicted).sum().item()

        epoch_duration = time.time() - since
        epoch_loss = running_loss / len(train_loader)
        epoch_acc = 100 / 32 * running_correct / len(train_loader)
        print("Epoch %s, duration: %d s, loss: %.4f, acc: %.4f" % (epoch + 1, epoch_duration, epoch_loss, epoch_acc))

        losses.append(epoch_loss)
        accuracies.append(epoch_acc)

        # Model needs to be switched to evaluation mode for evaluating the test data
        model.eval()
        test_acc = eval_model(model)
        test_accuracies.append(test_acc)

        # Re-set the model to train mode after validating
        model.train()
        scheduler.step(test_acc)

        since = time.time()
    print('Finished Training')
    return model, losses, accuracies, test_accuracies


def eval_model(model):
    correct = 0.0
    total = 0.0
    with torch.no_grad():
        for i, data in enumerate(test_loader, 0):
            images, labels = data
            images = images.to(device)
            labels = labels.to(device)

            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)

            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    test_acc = 100.0 * correct / total
    print('Accuracy of the network on the test images: %d %%' % test_acc)
    return test_acc


def find_classes(dir):
    classes = os.listdir(dir)
    classes.sort()
    class_to_idx = {classes[i]: i for i in range(len(classes))}
    return classes, class_to_idx


if __name__ == '__main__':
    resnet_model = models.resnet34(pretrained=True)
    log_event('resnet34 is loaded..')

    num_features = resnet_model.fc.in_features

    # Change the output class number to reflect how many car brands we are using. At the end, we will have 'output_class_number' output
    output_class_number = 3

    # replace the last fc layer with an untrained one (requires grad by default)
    resnet_model.fc = nn.Linear(num_features, output_class_number)
    resnet_model = resnet_model.to(device)

    # Assign training objects
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(resnet_model.parameters(), lr=0.01, momentum=0.9)
    lr_scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='max', patience=3, threshold=0.9)
    log_event('Training Objects have been created.')


    # Start the training:
    log_event('train_model(): Model training has started..')
    resnet_model, training_losses, training_accs, test_accs = train_model(resnet_model, criterion, optimizer, lr_scheduler, n_epochs=16)
    log_event('train_model(): Model training has finished..')

    prediction_model = torch.save(model_ft, f'prediction.model')
    log_event('prediction_model is saved.')

    # Uncomment below, if you need to see which classes in which index
    #classes, c_to_idx = find_classes(dataset_dir + "train")
    #print(classes, c_to_idx)
