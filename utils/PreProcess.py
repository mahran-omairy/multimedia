from imageai.Detection import ObjectDetection
import os

model_path = f"D:/multimedia_project/multimedia/car_site/models/yolo.h5"
source_directory = f'D:/multimedia_project/stanford-car-dataset-by-classes-folder/car_data/car_data/test/'


# source_directory = f'D:/multimedia_project/stanford-car-dataset-by-classes-folder/car_data/car_data/train/'

def load_model():
    """
        Crate and load ObjectDetection Model by using YOLOv3

    :return: YOLOv3 Object detector
    """
    detector_object = ObjectDetection()
    detector_object.setModelTypeAsYOLOv3()
    detector_object.setModelPath(model_path)
    detector_object.loadModel()
    return detector_object


def car_detector(source_directory_path, image_name, detector_obj):
    """
        Method will detect and extract cars from the given image by using the YOLOv3 detector
        :param source_directory_path: Directory path for input images
        :param image_name: Input image name
        :param detector_obj: Detector that was created by load_model()

    :return: paths for new image location, input_location, out_location
    """
    input_path = source_directory_path + image_name
    output_path = source_directory_path + f'cropped_' + image_name

    detector_obj.detectObjectsFromImage(input_image=input_path,
                                        output_image_path=output_path,
                                        minimum_percentage_probability=80,
                                        extract_detected_objects=True)

    return output_path + '-objects', input_path, output_path


if __name__ == '__main__':
    detector = load_model()
    counter = 0

    """
        1. Iterate through the source folder.
        2. Find a folder which is unique for a model.
        3. Iterate all images:
            3.1 Detect car, rename and save it as '<cropped_car> + <counter> + .jpg'
            3.2 Remove the original image so we only have the cropped ones left to use for training the network model
    """
    for folder_name in os.listdir(source_directory):
        folder_path = source_directory + folder_name + f'/'
        for car_image in os.listdir(folder_path):
            if car_image.endswith(".jpg"):
                new_loc, input_location, out_location = car_detector(source_directory_path=folder_path,
                                                                     image_name=car_image,
                                                                     detector_obj=detector)
                try:
                    for new_images in os.listdir(new_loc):
                        os.rename(new_loc + '/' + new_images, (folder_path + 'cropped_car' + str(counter) + '.jpg'))
                        counter += 1
                    os.remove(input_location)
                    os.remove(out_location)
                    os.removedirs(new_loc)
                except Exception as err:
                    print('No car detected..', err)

        print(folder_name)
